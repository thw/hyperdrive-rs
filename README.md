# hyperdrive.rs

rust implementation of dat:// related hyperdrive project. see github.com/mafintosh/hyperdrive

## dependencies
install rust. 

for sodium oxide:
`apt-get install libsodium-dev pkg-config`


## specs
This project aims to be an rust implementation of hypercore + hyperdrive mirroring https://github.com/mafintosh/hypercore
See detailed protocol specification at `dat-paper.pdf`
See DRAFT 1 of the protocol at https://www.datprotocol.com/spec.html



