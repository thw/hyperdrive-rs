use std::path::PathBuf;
use std::fs::OpenOptions;
use std::fs::remove_dir_all;
use std::io::prelude::*;

extern crate hyperdrive;
use hyperdrive::Feed;
use hyperdrive::file_to_bytes;

fn print_hex(bytes: &[u8]) { 
    for i in 0..bytes.len() {
        if (i % 8) == 0 {
            println!("");
        }
        print!("{:02x}  ", bytes[i]);
    }
    println!("");
}

fn test_files_equal(a: &PathBuf, b: &PathBuf, c: &str) {
    let mut a_path = PathBuf::new();
    a_path.push(a);
    a_path.push(c);
    assert!(a_path.exists(), format!("a_path ({:?}) does not exist", a_path.display()));

    let mut b_path = PathBuf::new();
    b_path.push(b);
    b_path.push(c);
    assert!(b_path.exists(), format!("b_path ({:?}) does not exist", b_path.display()));

    let a_bytes = file_to_bytes(a_path).unwrap();  
    let b_bytes = file_to_bytes(b_path).unwrap();  

    if (a_bytes.len() != b_bytes.len()) {
        println!("file '{}' sizes not equal!", c);
        println!("{}: {}", a.display(), a_bytes.len());
        print_hex(&a_bytes);
        println!("{}: {}", b.display(), b_bytes.len()); 
        print_hex(&b_bytes);
        assert!(false);
    }
}

/// for this test to pass, you need to run: 
/// `npm install hyperdrive && node hc.js`
#[test]
fn read_js_out() { 
    let mut hcjs_out = PathBuf::new();
    hcjs_out.push("/tmp/hcjs-test");
    let js_code = ["hc = require('hypercore');\n",
                    "opts = { valueEncoding: 'binary' };\n",
                    "f = hc('/tmp/hcjs-test', opts);\n",
                    "f.append('hola');\n",
                    "f.append('mundo');\n"];
    let mut js_path = PathBuf::new();
    js_path.push("hc.js");
    let mut js_file = OpenOptions::new()
                        .create(true)
                        .write(true)
                        .open(js_path).unwrap();
    for jc in js_code.iter() { 
        js_file.write_all(jc.as_bytes()).unwrap();
    }

    let mut feed_out = PathBuf::new();
    feed_out.push("/tmp/hcrs-test");
    if feed_out.exists() { 
        assert!(remove_dir_all(&feed_out).is_ok());
    }
    let mut feed_out_dup = PathBuf::new(); 
    feed_out_dup.push("/tmp/hcrs-test");
    let mut feed = Feed::new(feed_out_dup);
    assert!(feed.append("hola".as_bytes()).is_ok());
    assert!(feed.append("mundo".as_bytes()).is_ok());
    assert!(feed.write().is_ok());

    test_files_equal(&hcjs_out, &feed_out, "data");
    test_files_equal(&hcjs_out, &feed_out, "signatures");
    test_files_equal(&hcjs_out, &feed_out, "tree");
    test_files_equal(&hcjs_out, &feed_out, "bitfield");

    let feed2 = Feed::read(hcjs_out).unwrap();
    assert!(feed2.exists(), "this test requires a hypercore feed file");
}

