extern crate num; 
use self::num::Integer;

/// Provides and index based storage method for implementing a tree in a
/// flat storage space such as a Vec. Nodes in this tree have an index, 
/// depth and offset. 
/// 
/// index: the linear index into the memory area for a node. 
/// depth: the number of ancestry steps below this node
/// offset: the number of nodes of the same depth with a lower index 
///
/// depth -->    
/// o 0
/// f  1
/// f 2 
/// s   3
/// e 4
/// t  5 
/// | 6
/// v       
///   8
///    9
///   10
pub struct FlatTree {
}

impl FlatTree {
    /// returns linear index for a node of particular depth and offset
    pub fn index(depth: u64, offset: u64) -> u64 {
        (1 + 2 * offset) * 2u64.pow(depth as u32) - 1
    }

    /// calculate the depth for an index
    pub fn depth(index: u64) -> u64 {
        let mut depth = 0;
        let mut idx = index;
        idx += 1;
        while idx.is_even() {
            depth += 1;
            idx = idx >> 1;
        }
        depth
    }

    /// calcualte the offset for a particular index 
    pub fn offset(index: u64, depth: u64) -> u64 {
        if index.is_even() {
            return index / 2;
        }
        ((index + 1) / 2u64.pow(depth as u32) - 1) / 2
    }

    /// calculate index of parent node 
    pub fn parent(index: u64) -> u64 {
        let depth = FlatTree::depth(index);
        let offset = FlatTree::offset(index, depth); 
        FlatTree::index(depth + 1, offset >> 1)
    }

    /// return list of children or None
    pub fn children(index: u64) -> Option<Vec<u64>> {
        if index.is_even() {
            None
        } else {
            let d = FlatTree::depth(index);
            let off = FlatTree::offset(index, d);
            Some(vec![FlatTree::index(d - 1, 2 * off), 
                    FlatTree::index(d - 1, 2 * off + 1)])
        }
    }

    /// returns the left child
    pub fn left_child(index: u64) -> Option<u64> {
        if FlatTree::depth(index) == 0 {
            None
        } else {
            Some(FlatTree::index(FlatTree::depth(index) - 1, 2 * FlatTree::offset(index, FlatTree::depth(index))))
        }
    }
    /// returns the right child
    pub fn right_child(index: u64) -> Option<u64> {
        if FlatTree::depth(index) == 0 {
            None
        } else {
            Some(FlatTree::index(FlatTree::depth(index) - 1, 2 * FlatTree::offset(index, FlatTree::depth(index)) + 1))
        }
    }


    /// returns sibling for a nodes
    pub fn sibling(index: u64) -> u64 {
        let d = FlatTree::depth(index);
        let off = FlatTree::offset(index, d);
        if off.is_odd() {
            FlatTree::index(d, off - 1)
        } else {
            FlatTree::index(d, off + 1)
        }
    }

    /// provides a list of all roots
    pub fn full_roots(index: u64) -> Vec<u64> {
        assert!(index.is_even());
        let mut roots = Vec::new();
        let mut idx = index / 2;
        let mut offset = 0;
        let mut factor = 1;
        loop { 
            if idx == 0 {
                return roots;
            }
            while 2 * factor <= idx {
                factor *= 2
            }
            println!("{}, {}, {}", idx, factor, offset);
            roots.push(offset + factor - 1);
            offset = offset + 2 * factor;
            idx -= factor;
            factor = 1;
        }   
    }

}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn base_blocks() {
        assert_eq!(0, FlatTree::index(0, 0));
        assert_eq!(2, FlatTree::index(0, 1));
        assert_eq!(4, FlatTree::index(0, 2));
        assert_eq!(6, FlatTree::index(0, 3));
        assert_eq!(8, FlatTree::index(0, 4));
        assert_eq!(10, FlatTree::index(0, 5));
    }

    #[test]
    fn parents() {
        assert_eq!(1, FlatTree::index(1, 0));
        assert_eq!(5, FlatTree::index(1, 1));
        assert_eq!(9, FlatTree::index(1, 2));
        
        assert_eq!(1, FlatTree::parent(0));
        assert_eq!(1, FlatTree::parent(2));
        assert_eq!(3, FlatTree::parent(1));
        assert_eq!(7, FlatTree::parent(3));
    }

    #[test]
    fn depth() {
        assert_eq!(0, FlatTree::depth(0));
        assert_eq!(1, FlatTree::depth(1));
        assert_eq!(0, FlatTree::depth(2));
        assert_eq!(2, FlatTree::depth(3));
        assert_eq!(0, FlatTree::depth(4));
        assert_eq!(1, FlatTree::depth(5));
        assert_eq!(0, FlatTree::depth(6));
        assert_eq!(3, FlatTree::depth(7));
        assert_eq!(0, FlatTree::depth(8));
        assert_eq!(1, FlatTree::depth(9));
        assert_eq!(0, FlatTree::depth(10));
        assert_eq!(2, FlatTree::depth(11));
        assert_eq!(0, FlatTree::depth(12));
    }

    #[test]
    fn children() { 
        assert!(FlatTree::children(0).is_none());
        assert!(FlatTree::children(2).is_none());
        assert!(FlatTree::children(8).is_none());
        assert_eq!(vec![0, 2], FlatTree::children(1).unwrap());
        assert_eq!(vec![1, 5], FlatTree::children(3).unwrap());
        assert_eq!(vec![4, 6], FlatTree::children(5).unwrap());
    }

    #[test]
    fn left_child() {
        assert!(FlatTree::left_child(0).is_none());
        assert!(FlatTree::left_child(4).is_none());
        assert_eq!(0, FlatTree::left_child(1).unwrap());
        assert_eq!(1, FlatTree::left_child(3).unwrap());
        assert_eq!(3, FlatTree::left_child(7).unwrap());
    }

    #[test]
    fn right_child() {
        assert!(FlatTree::right_child(0).is_none());
        assert!(FlatTree::right_child(4).is_none());
        assert_eq!(2, FlatTree::right_child(1).unwrap());
        assert_eq!(5, FlatTree::right_child(3).unwrap());
        assert_eq!(11, FlatTree::right_child(7).unwrap());
    }

    #[test]
    fn siblings() { 
        assert_eq!(2, FlatTree::sibling(0));
        assert_eq!(0, FlatTree::sibling(2));
        assert_eq!(5, FlatTree::sibling(1));
        assert_eq!(1, FlatTree::sibling(5));
    }

    #[test]
    fn full_roots() {
        let empty: Vec<u64> = Vec::new();
        assert_eq!(empty, FlatTree::full_roots(0));
        assert_eq!(vec![0], FlatTree::full_roots(2));
        assert_eq!(vec![3], FlatTree::full_roots(8));
        assert_eq!(vec![7, 17], FlatTree::full_roots(20));
        assert_eq!(vec![7, 16], FlatTree::full_roots(18));
        assert_eq!(vec![7], FlatTree::full_roots(16));
    }
}
