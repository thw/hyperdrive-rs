use std::path::PathBuf;
use std::fs::{File, DirBuilder, OpenOptions};
use std::fs::{remove_dir_all, create_dir_all};
use std::io;
use std::io::SeekFrom;
use std::io::prelude::*;

extern crate byteorder;
extern crate sodiumoxide;
extern crate libsodium_sys;

// pubkey signing via sodiumoxide
use sodiumoxide::crypto::sign;
use sodiumoxide::crypto::sign::ed25519::PublicKey;
use sodiumoxide::crypto::sign::ed25519::PUBLICKEYBYTES;
use sodiumoxide::crypto::sign::ed25519::SecretKey;
use sodiumoxide::crypto::sign::ed25519::SECRETKEYBYTES;
use sodiumoxide::crypto::sign::ed25519::SIGNATUREBYTES;

mod hashes;
use hashes::{bitfield_header, signatures_header, tree_header};
use hashes::{encode_u64be, tree_hash, TREE_HASH_LEN, SIGNATURE_LEN};

mod merkle;
mod flattree;
use merkle::MerkleTree;
use merkle::Node;

#[derive(Default)]
struct BitField {
}

impl BitField { 
    /// TODO 
    pub fn set(&self, index: usize, value: bool) {
    }
}

#[derive(Default)]
struct Tree { 
} 

impl Tree { 
    /// TODO 
    pub fn set(&self, index: usize) {
    }
}

pub fn file_to_bytes(src: PathBuf) -> Result<Vec<u8>, io::Error> {
    let b_file = File::open(src)?;
    let mut bytes = Vec::new();
    for byte in b_file.bytes() { 
        bytes.push(byte?);
    }
    Ok(bytes)
}

pub fn file_to_bytes_check(src: PathBuf, len: usize) -> Result<Vec<u8>, io::Error> {
    let bytes = file_to_bytes(src)?;
    if bytes.len() != len {
        return Err(io::Error::new(io::ErrorKind::Other, "")) 
    }
    Ok(bytes)
}

fn write_header(path: PathBuf, bytes: &[u8]) -> Result<(), io::Error> {
    let mut b_file = OpenOptions::new()
                        .create(true)
                        .write(true)
                        .open(path)?;
    b_file.write_all(bytes)?;
    Ok(())
}
        
/// An append only binary data store serialized into the `directory`
/// directory. Can be read from file or created new. Newly created 
/// Feeds don't test for exiting files. Any io function will first
/// ensure the feed has been opened and validated or created.
pub struct Feed {
    directory: PathBuf,
    public_key: PublicKey,
    secret_key: SecretKey,
    merkle: MerkleTree,
    opened: bool,
    length: usize,
    byte_length: usize,
    bitfield: BitField,
    tree: Tree, 
}

impl Feed { 
    /// create a new feed for a specified directory. 
    pub fn new(directory: PathBuf) -> Feed {
        let (pk, sk) = sign::gen_keypair();
        Feed {
            directory: directory,
            public_key: pk,
            secret_key: sk,
            merkle: Default::default(),
            opened: false,
            length: 0,
            byte_length: 0,
            bitfield: Default::default(),
            tree: Default::default(),

        }
    }

    /// ensure that the feed is ready
    fn ensure_open(&mut self) -> Result<(), io::Error> {
        if self.opened { 
            return Ok(())
        }
        if self.directory.exists() {
            // we could also attempt to open an existing feed here...
            remove_dir_all(&self.directory)?;
        }   
        create_dir_all(&self.directory)?;
        // write some headers
        write_header(self.dat_path("signatures"), &signatures_header())?;
        write_header(self.dat_path("tree"), &tree_header())?;
        write_header(self.dat_path("bitfield"), &bitfield_header())?;
        self.opened = true;
        Ok(())
    }

    /// provides the offset into file `data` for given index
    /// SeekFrom wants u64, so we shall provide
    fn data_offset(&self, index: usize) -> u64 { 
        0
    }
    fn data_size(&self, index: usize) -> usize { 
        0
    }

    /// append the raw data to the `data` file
    fn write_data(&mut self, data: &[u8]) -> Result<(), io::Error> {
        let path = self.dat_path("data");
        let mut b_file = OpenOptions::new()
                            .append(true)
                            .create(true)
                            .open(path)?;
        b_file.write_all(data)?;
        Ok(())
    }

    /// write data at the correct offset into `data`
    fn write_data_index(&mut self, data: &[u8], index: usize) -> Result<(), io::Error> {
        let path = self.dat_path("data");
        let mut b_file = OpenOptions::new()
                            .append(true)
                            .create(true)
                            .open(path)?;
        let data_offset = self.data_offset(index);
        b_file.seek(SeekFrom::Start(data_offset))?; 
        b_file.write_all(data)?;
        Ok(())
    }

    /// grab data at index
    fn get_data(&self, index: usize) -> Result<Vec<u8>, io::Error> { 
        let path = self.dat_path("data");
        let mut b_file = OpenOptions::new().read(true).open(path)?;
        let data_offset = self.data_offset(index);
        let data_size = self.data_size(index);
        let mut buf : Vec<u8> = Vec::new();
        buf.resize(data_size, 0); 
        b_file.seek(SeekFrom::Start(data_offset))?; 
        b_file.read_exact(&mut buf)?;
        Ok(buf) 
    }

    /// write hash and size into `tree` file
    fn put_node(&mut self, node: Node) -> Result<(), io::Error> { 
        let mut buf = Vec::new();
        buf.extend(node.hash.iter().cloned());
        buf.extend(encode_u64be(node.size).iter().cloned());
        assert_eq!(TREE_HASH_LEN, buf.len() as u64);

        let path = self.dat_path("tree");
        let mut file = OpenOptions::new()
                            .append(true)
                            .create(true)
                            .open(path)?;

        // offset = header + 40 per node
        let offset = (tree_header().len() as u64) + (TREE_HASH_LEN * node.index);
        file.seek(SeekFrom::Start(offset))?;
        file.write_all(&buf)?;
        Ok(())
    }

    /// TODO
    fn put_signature(&mut self, index: u64, sig: &[u8]) -> Result<(), io::Error> {
        let path = self.dat_path("signatures");
        let mut file = OpenOptions::new()
                            .append(true)
                            .create(true)
                            .open(path)?;
        
        let offset = signatures_header().len() as u64 + (index * SIGNATURE_LEN);

        let file_size = file.metadata()?.len();
        println!("put_signature, current len {}, index {}, offset {}, sig len {}", file_size, index, offset, sig.len());

        file.seek(SeekFrom::Start(offset as u64))?;
        file.write_all(&sig);
        Ok(()) 
    }

    /// TODO
    /// announce to peers that a new data block is available
    fn announce_one(&self) {
    }   



    /// append a block of data to the feed
    /// see line 950 of hc/index.js
    pub fn append(&mut self, data: &[u8]) -> Result<(), io::Error> {
        self.ensure_open()?;
        self.write_data(data)?;
        let byte_count = data.len();
        
        let nodes = self.merkle.next(data);
        for node in nodes { 
            self.put_node(node)?;
        }

        let sig = sign::sign_detached(&tree_hash(&self.merkle.roots()), &self.secret_key);
        let index: u64 = self.length as u64;
        self.put_signature(index, &sig[..])?; 

        // TODO let start = self.length;
        self.byte_length += byte_count;
        self.bitfield.set(self.length, true);
        self.tree.set(2 * self.length);
        self.length += 1;  

        self.announce_one(); // (start, self.length);
        Ok(()) 
    }


    /// return a path for the sub file for this feed
    fn dat_path(&self, file_name: &str) -> PathBuf { 
        let mut hd_path = self.directory.clone();
        // this is how hyperdrive works, not hypercore: hd_path.push(".dat");
        hd_path.push(file_name);
        hd_path
    }

    /// write raw bytes to file
    fn bytes_to_file(dest: PathBuf, bytes: &[u8]) -> Result<(), io::Error> {
        let mut b_file = File::create(dest)?;
        b_file.write_all(bytes)?;
        Ok(())
    }

    /// write the feed to disk as 6 binary files:
    /// bitfield, data, key, secret_key, signatures, tree
    pub fn write(&self) -> Result<(), io::Error> { 
        let dat_dir = self.dat_path("");
        DirBuilder::new().recursive(true).create(dat_dir)?;

        Feed::bytes_to_file(self.dat_path("key"), &self.public_key[0..PUBLICKEYBYTES])?;
        Feed::bytes_to_file(self.dat_path("secret_key"), &self.secret_key[0..SECRETKEYBYTES])?;

        Ok(())
    }   


    /// return a newly read and validated feed
    pub fn read(directory: PathBuf) -> Result<Feed, io::Error> { 
        let mut feed = Feed::new(directory);
        feed._read()?;
        Ok(feed)
    }

    fn _read(&mut self) -> Result<(), io::Error> {
        let pk_bytes = file_to_bytes_check(self.dat_path("key"), PUBLICKEYBYTES)?;
        match PublicKey::from_slice(&pk_bytes) {
            Some(pk) => { self.public_key = pk },
            None => { return Err(io::Error::new(io::ErrorKind::Other, "unable to read key")); }
        }
        let sk_bytes = file_to_bytes_check(self.dat_path("secret_key"), SECRETKEYBYTES)?;
        match SecretKey::from_slice(&sk_bytes) {
            Some(sk) => { self.secret_key = sk },
            None => { return Err(io::Error::new(io::ErrorKind::Other, "unable to read secret key")); }
        }
        println!("_read success");
        Ok(())
    }

    pub fn exists(&self) -> bool {
        self.directory.exists()
    }

}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn basic_write() {
        let mut directory = PathBuf::new();
        directory.push("/tmp");
        directory.push("hd-unit");
       
        if directory.exists() { 
            assert!(remove_dir_all(&directory).is_ok());
        }

        let mut feed = Feed::new(directory);

        assert!(feed.append("hola".as_bytes()).is_ok());
        assert!(feed.append("mundo".as_bytes()).is_ok());

        assert!(feed.write().is_ok());
        println!("directory exists? {0}", feed.exists());
        assert_eq!(true, feed.exists());
    }

    fn other_len(b: &[u8]) -> usize {
        b.len() 
    }

    #[test]
    fn sig_len() { 
        let (pk, sk) = sign::gen_keypair(); 
        let bytes = vec![0, 1, 2, 3, 4, 5, 6, 7];
        println!("crypto sign bytes {}", SIGNATUREBYTES);
        let sig = sign::sign_detached(&bytes, &sk);
        let sig_bytes = other_len(&sig[..]);
        assert_eq!(64, sig_bytes);
    }
}

