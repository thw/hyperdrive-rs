//use std::default::Default;

//use super::flattree;
use flattree::FlatTree;
use hashes::encode_u64be;
use hashes::blake2b_batch;
use hashes::LEAF_TYPE;
use hashes::PARENT_TYPE;

pub type HashInt = u64;
pub type IndexInt = u64;
pub type Content = Vec<u8>;

#[derive(Clone, Default, Debug)]
pub struct Node {
    pub index: u64, 
    pub parent: u64,
    pub hash: Vec<u8>,
    pub data: Vec<u8>,
    pub size: u64, 
}

/// merkle tree that supports stream style usage
#[derive(Default)]
pub struct MerkleTree {
    blocks: u64,
    roots: Vec<Node>,
}

pub fn leaf_hash(data: &[u8]) -> Vec<u8> {         
    blake2b_batch(vec![LEAF_TYPE.to_vec(), 
                encode_u64be(data.len() as u64), 
                data.to_vec()]).unwrap()
}
pub fn parent_hash(left: &Node, right: &Node) -> Vec<u8> {
    blake2b_batch(vec![PARENT_TYPE.to_vec(), 
                encode_u64be(left.size + right.size),
                left.hash.clone(), 
                right.hash.clone()]).unwrap()
}

impl MerkleTree {
    pub fn new() -> MerkleTree {
        MerkleTree {
            blocks: 0,
            roots: vec![],
        }
    }   

    /// append data to the tree. update roots and return nodes that 
    /// need to be stored. the list of returned nodes is the path
    /// from the inserted data leaf to a root node in the merkle tree 
    pub fn next(&mut self, data: &[u8]) -> Vec<Node> {
        let mut nodes = vec![];
        let index = 2 * self.blocks;
        self.blocks += 1;

        let leaf = Node { 
            index: index,
            parent: FlatTree::parent(index),
            hash: leaf_hash(data), 
            data: data.to_vec(),
            size: data.len() as u64,
        };

        // new data nodes are always leaves. they could also be
        // roots if the siblings aren't full yet. accumulate the
        // path from new leaf to root into `nodes`, while updating 
        // the current roots.         

        nodes.push(leaf.clone());

        if self.roots.len() == 0 { 
            self.roots.push(leaf.clone());
        } else { 
            let mut right = leaf.clone();
            let mut consider_leaf = true;

            while self.roots.len() > 1 || consider_leaf {
                consider_leaf = false;
                let root_count = self.roots.len();
                let left = self.roots[root_count - 1].clone();

                // in the case where right is now a root
                if left.parent != right.parent {
                    self.roots.push(right.clone());
                    break;
                }

                // otherwise, we shared a parent with the previous last
                // root, so update the roots list 
                // and include this new root/partial root in the path
                let new_last_root = Node { 
                    index: left.parent,
                    parent: FlatTree::parent(left.parent),
                    hash: parent_hash(&left, &right), 
                    size: left.size + right.size,
                    data: vec![],
                };
                self.roots[root_count - 1] = new_last_root.clone();
                nodes.push(new_last_root.clone()); 
                right = new_last_root;
            }
        }

        nodes 
    } 

    /// current roots
    pub fn roots(&self) -> Vec<Node> {
        self.roots.clone()
    }
}

