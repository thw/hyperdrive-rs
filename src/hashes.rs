use std::mem;
use std::ptr;

use byteorder::{BigEndian, WriteBytesExt};

// generichash functions haven't yet been lifted into 
// rusty style sodiumoxide, so use the ffi instead 
use libsodium_sys::crypto_generichash_state;
use libsodium_sys::crypto_generichash_statebytes;
use libsodium_sys::crypto_generichash_init;
use libsodium_sys::crypto_generichash_update;
use libsodium_sys::crypto_generichash_final;


use merkle::Node;

// serialization constants
pub const LEAF_TYPE: [u8; 1] = [0];
pub const PARENT_TYPE: [u8; 1] = [1u8];
pub const ROOT_TYPE: [u8; 1] = [2u8];
pub const HYPERCORE: [u8; 9] = ['h' as u8, 
                            'y' as u8, 
                            'p' as u8, 
                            'e' as u8, 
                            'r' as u8, 
                            'c' as u8, 
                            'o' as u8, 
                            'r' as u8, 
                            'e' as u8];

/// provides a hashed value of the tree's roots
pub fn tree_hash(roots: &[Node]) -> Vec<u8> { 
    let mut bufs : Vec<Vec<u8>> = Vec::new();
    bufs.push(ROOT_TYPE.to_vec());

    for root in roots { 
        bufs.push(root.hash.clone());
        bufs.push(encode_u64be(root.index));
        bufs.push(encode_u64be(root.size));
    }
    
    blake2b_batch(bufs).unwrap()
}

/// a 32byte header 
fn header(header_type: u8, size: u16, name: &str) -> Vec<u8> {
    assert!(name.len() < <u8>::max_value() as usize, "name must be shorter");
    let mut h = vec![5, 2, 87, header_type];
    let version = 0u8;
    h.push(version);
    h.write_u16::<BigEndian>(size);
    h.push(name.len() as u8);
    h.extend(name.as_bytes());
    h.resize(32, 0u8);  
    h   
}
pub const TREE_HASH_LEN: u64 = 40;
pub fn tree_header() -> Vec<u8> { 
    header(2, TREE_HASH_LEN as u16, "BLAKE2b")
}
pub const SIGNATURE_LEN: u64 = 64;
pub fn signatures_header() -> Vec<u8> { 
    header(1, SIGNATURE_LEN as u16, "Ed25519")
}
pub const BITFIELD_LEN: u64 = 3328;
pub fn bitfield_header() -> Vec<u8> {
    header(0, BITFIELD_LEN as u16, "")
}
    
/// perform a batch based hash of a list of buffers. 
/// use no key and select a 32 byte output length to match hypercore-js
/// we call it blake2b explicitly as hypercore-js does, even though
/// perhaps an update to libsodium might make this another primitive
pub fn blake2b_batch(buffers: Vec<Vec<u8>>) -> Result<Vec<u8>, ()> {
    let mut out = vec![0u8; 32];
    let mut st = vec![0u8; (unsafe { crypto_generichash_statebytes() })];
    let pst = unsafe { mem::transmute::<*mut u8, *mut crypto_generichash_state>(st.as_mut_ptr()) };

    if 0 != unsafe { crypto_generichash_init(pst, 
                        ptr::null(), 0, out.len()) } { 
        return Err(());
    }
    
    for buf in buffers { 
        if 0 != unsafe { crypto_generichash_update(pst, 
                            buf.as_ptr(), buf.len() as u64) } { 
            return Err(());
        }
    } 

    if 0 != unsafe { crypto_generichash_final(pst, 
                        out.as_mut_ptr(), out.len()) } {
        return Err(());
    }

    Ok(out) 
}

pub fn encode_u64be(x: u64) -> Vec<u8> {
    let mut enc = vec![];
    enc.write_uint::<BigEndian>(x, 8);
    enc
}

#[cfg(test)]
mod tests { 
    use super::*;
    
    #[test]
    fn blake_batch() { 
        let a = vec![0xFFu8, 0x00, 0xFF, 0x00];
        let b = vec![0x55u8, 0xAA, 0x55, 0xAA, 0xFF, 0x00, 0x00];
        let c = vec![0x55u8, 0xAA, 0x55, 0xAA, 0xFF, 0x00, 0x00];
        let d = vec![0x55u8, 0xAA, 0x55, 0xAA, 0xFF, 0x00, 0x00];
        let buffers = vec![a, b, c, d];
        let digest = blake2b_batch(buffers).unwrap();
        assert_eq!(32, digest.len());
    }

    #[test]
    fn header_lens() { 
        assert_eq!(32, tree_header().len());
        assert_eq!(32, signatures_header().len());
        assert_eq!(32, bitfield_header().len());
    }

    #[test]
    fn sig_header() { 
        assert_eq!(vec![0x05, 0x02, 0x57, 0x01, 0x00, 0x00, 0x40, 0x07,
                        0x45, 0x64, 0x32, 0x35, 0x35, 0x31, 0x39, 0x00,
                        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
                    signatures_header());
    }   

    #[test]
    fn be64() {
        assert_eq!(vec![0x55, 0x33, 0x44, 0x22, 0xAA, 0xCC, 0xDD, 0xEE],
                    encode_u64be(0x55334422AACCDDEE));
    }
}

