use std::path::PathBuf;

extern crate hyperdrive;
use hyperdrive::Feed;




fn main() {
    println!("Hello, world!");

    let mut storage = PathBuf::new();
    storage.push("/tmp");
    storage.push("lettuce-live_in_tokyo");
    let mut feed = Feed::new(storage);

    assert!(feed.append("hola".as_bytes()).is_ok());
    assert!(feed.append("mundo".as_bytes()).is_ok());

    println!("storage exists? {0}", feed.exists());
    match feed.write() {
        Err(why) => println!("couldn't write feed: {}", why),
        Ok(_x) => ()
    }
}
